<?php

declare(strict_types=1);

namespace App\Factory;

use App\Trip;

class TripFactory
{
    public function createTrip(array $data): Trip
    {
        return Trip::create($data);
    }
}
