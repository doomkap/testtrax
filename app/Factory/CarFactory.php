<?php

declare(strict_types=1);

namespace App\Factory;

use App\Car;

class CarFactory
{
    public function createCar(array $data): Car
    {
        return Car::create($data);
    }
}
