<?php

namespace App\Policies;

use App\Car;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CarPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can delete the model.
     *
     * @param  User  $user
     * @param  Car  $car
     *
     * @return bool
     */
    public function delete(User $user, Car $car): bool
    {
        return $user->id === $car->user_id;
    }

}
