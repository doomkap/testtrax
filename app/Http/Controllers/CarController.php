<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Car;
use App\Factory\CarFactory;
use App\Http\Requests\CarRequest;
use App\Http\Resources\CarCollection;
use App\Http\Resources\CarResource;
use Illuminate\Support\Facades\Gate;
use Symfony\Component\HttpFoundation\JsonResponse;

class CarController
{
    public function index(): JsonResponse
    {
        $cars = Car::all();

        return response()->json(new CarCollection($cars));
    }

    public function show(int $id): JsonResponse
    {
        $car = Car::findOrFail($id);
        $carResource = (new CarResource($car))->withTrips();

        return response()->json(['data' => $carResource]);
    }

    public function store(CarRequest $request, CarFactory $carFactory): JsonResponse
    {
        $car = $carFactory->createCar($request->validated());

        return response()->json(new CarResource($car));
    }

    public function delete(int $id): JsonResponse
    {
        $car = Car::findOrFail($id);
        if (Gate::denies('delete', $car)) {
            abort(403);
        };

        $car->delete();

        return response()->json(['message' => 'Car removed successfully']);
    }
}
