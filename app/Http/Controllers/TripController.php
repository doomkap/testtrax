<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Factory\TripFactory;
use App\Http\Requests\TripRequest;
use App\Http\Resources\TripCollection;
use App\Http\Resources\TripResource;
use App\Trip;
use Symfony\Component\HttpFoundation\JsonResponse;

class TripController
{
    public function index(): JsonResponse
    {
        $trips = Trip::orderByDesc('date')->get();

        return response()->json(new TripCollection($trips));
    }

    public function store(TripRequest $request, TripFactory $tripFactory): JsonResponse
    {
        $trip = $tripFactory->createTrip($request->validated());

        return response()->json(new TripResource($trip));
    }
}
