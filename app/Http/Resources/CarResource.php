<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CarResource extends JsonResource
{
    private $trips = false;

    public function toArray($request): array
    {
        $data = [
            'id' => $this->resource->id,
            'year' => $this->resource->year,
            'make' => $this->resource->make,
            'model' => $this->resource->model,
        ];

        if ($this->trips) {
            $data['trip_count'] = $this->resource->trips()->count();
            $data['trip_miles'] = (int) $this->resource->trips()->sum('miles');
        }

        return $data;
    }

    public function withTrips(): self
    {
        $this->trips = true;

        return $this;
    }
}
