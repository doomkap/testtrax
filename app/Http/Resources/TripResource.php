<?php

declare(strict_types=1);

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class TripResource extends JsonResource
{
    public function toArray($request): array
    {
        return [
            'id' => $this->resource->id,
            'date' => (new Carbon($this->resource->date))->format('m/d/Y'),
            'miles' => $this->resource->miles,
            'total' => $this->resource->miles_balance,
            'car' => new CarResource($this->resource->car)
        ];
    }
}
