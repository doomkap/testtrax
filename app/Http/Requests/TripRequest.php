<?php

declare(strict_types=1);

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class TripRequest extends FormRequest
{
    public function validationData(): array
    {
        $this->merge(
            [
                'user_id' => Auth::id(),
            ]
        );

        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'date' => 'required|date',
            'car_id' => 'required|numeric|exists:cars,id',
            'miles' => 'required|numeric'
        ];
    }
}
