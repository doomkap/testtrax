<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class CarRequest extends FormRequest
{
    public function validationData(): array
    {
        $this->merge(
            [
                'user_id' => Auth::id(),
            ]
        );

        return $this->all();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'user_id' => 'required|exists:users,id',
            'year' => 'required|digits:4',
            'make' => 'required|string|max:255',
            'model' => 'required|string|max:255'
        ];
    }
}
