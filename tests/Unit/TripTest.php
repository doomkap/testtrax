<?php

namespace Tests\Unit;

use App\Factory\TripFactory;
use App\Trip;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class TripTest extends TestCase
{
    private $data = [
        'user_id' => 1,
        'date' => '2022-02-01T00:00:00.000+01:00', // ISO 8601 string
        'car_id' => '1',
        'miles' => '12'
    ];

    public function testCreateTrip()
    {
        DB::beginTransaction();
        $carFactory = new TripFactory();

        $this->assertInstanceOf(Trip::class, $carFactory->createTrip($this->data));
        DB::rollback();
    }
}
