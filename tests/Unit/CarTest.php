<?php

namespace Tests\Unit;

use App\Car;
use App\Factory\CarFactory;
use App\Http\Resources\CarResource;
use Illuminate\Support\Facades\DB;
use Symfony\Component\HttpFoundation\Request;
use Tests\TestCase;

class CarTest extends TestCase
{
    private $data = [
        'user_id' => 1,
        'year' => '2000',
        'make' => 'Ford',
        'model' => 'Fiesta'
    ];

    public function testMakeCar()
    {
        DB::beginTransaction();
        $carFactory = new CarFactory();

        $this->assertInstanceOf(Car::class, $carFactory->createCar($this->data));
        DB::rollback();
    }

    public function testCarResource()
    {
        DB::beginTransaction();
        $carFactory = new CarFactory();
        $car = $carFactory->createCar($this->data);
        $car->id = 1;

        $resource = new CarResource($car);
        $request  = Request::create('/api/car/1', 'GET');

        $expectedData = ['data' => [
            "id" => 1,
            "year" => "2000",
            "make" => "Ford",
            "model" => "Fiesta",
        ]];

        $this->assertEquals($expectedData, $resource->response($request)->getData(true));
        DB::rollback();
    }
}
